# sqliteStorage

SQLite implementation of the [`Storage`](https://developer.mozilla.org/en-US/docs/Web/API/Storage) interface. A drop-in replacement for `localStorage`.

## Why?

`localStorage` has a 10MB limit. Also, since [Deno implements `localStorage`](https://github.com/denoland/deno/pull/7819), it's practically _begging_ for code to be written against the `Storage` interface. This gives the flexibility to switch to a different backend (eg Redis, Postgres) with minimal code changes, only needing to implement a `Storage` adapter.

## Usage

```ts
import SqliteStorage from 'https://gitlab.com/soapbox-pub/sqlite-storage/-/raw/v1.0.0/mod.ts';

const sqliteStorage = new SqliteStorage();

sqliteStorage.setItem('green', 'luigi');
sqliteStorage.setItem('red', 'mario');

if (sqliteStorage.length > 1 && sqliteStorage.key(0) === 'green') {
  sqliteStorage.clear();
}
```

Options:

- `new SqliteStorage('./db.sqlite3')` - provide a path to the database. If left empty, the database will run in-memory.
- `new SqliteStorage('./db.sqlite3', { mode: 'read' })` - open an existing database in read-only mode.

## About

This module uses Deno [`sqlite`](https://deno.land/x/sqlite) internally.

The SQL statements were adapted from [Deno's native WebStorage implementation](https://github.com/denoland/deno/pull/7819).

## License

This is free and unencumbered software released into the public domain.
