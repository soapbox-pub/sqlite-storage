import { assertEquals } from 'https://deno.land/std@0.177.0/testing/asserts.ts';

import SqliteStorage from './mod.ts';

Deno.test('sqliteStorage', () => {
  const sqliteStorage = new SqliteStorage();

  sqliteStorage.setItem('hello', 'world');

  assertEquals(sqliteStorage.length, 1);
  assertEquals(sqliteStorage.getItem('hello'), 'world');
  assertEquals(sqliteStorage.getItem('world'), null);

  sqliteStorage.clear();

  assertEquals(sqliteStorage.length, 0);

  sqliteStorage.setItem('green', 'luigi');
  sqliteStorage.setItem('red', 'mario');

  assertEquals(sqliteStorage.key(1), 'red');

  sqliteStorage.removeItem('green');

  assertEquals(sqliteStorage.key(1), null);
  assertEquals(sqliteStorage.key(0), 'red');
});
