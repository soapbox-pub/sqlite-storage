import { DB, SqliteOptions } from 'https://deno.land/x/sqlite@v3.7.0/mod.ts';

class SqliteStorage implements Storage {
  #db: DB;

  constructor(path?: string, options?: SqliteOptions) {
    this.#db = new DB(path, options);

    this.#db.execute(`
      CREATE TABLE IF NOT EXISTS data (
        key VARCHAR UNIQUE,
        value VARCHAR
      )
    `);
  }

  get length() {
    const result = this.#db.query('SELECT COUNT(*) FROM data');
    return result[0][0] as number;
  }

  key(index: number): string | null {
    const result = this.#db.query('SELECT key FROM data LIMIT 1 OFFSET ?', [index]);
    return result[0] ? result[0][0] as string : null;
  }

  getItem(key: string): string | null {
    const result = this.#db.query('SELECT value FROM data WHERE key = ?', [key]);
    return result[0] ? result[0][0] as string : null;
  }

  setItem(key: string, value: string): void {
    this.#db.query('INSERT OR REPLACE INTO data (key, value) VALUES (?, ?)', [key, value]);
  }

  removeItem(key: string): void {
    this.#db.query('DELETE FROM data WHERE key = ?', [key]);
  }

  clear(): void {
    this.#db.query('DROP TABLE data');
    this.#db.query('CREATE TABLE data (key VARCHAR UNIQUE, value VARCHAR)');
  }
}

export default SqliteStorage;
